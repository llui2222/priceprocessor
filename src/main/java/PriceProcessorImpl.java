import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class PriceProcessorImpl implements PriceProcessor {
    private static final int MAX_CCY_PAIRS_NUMBER = 2048;

    private final Set<PriceProcessor> priceProcessors = ConcurrentHashMap.newKeySet();
    private final Map<String, Double> lots = new ConcurrentHashMap<>();
    private final Map<String, Boolean> ccyPairAlreadyInQueue = new ConcurrentHashMap<>();
    private final BlockingQueue<String> ccyPairsChanged = new ArrayBlockingQueue<>(MAX_CCY_PAIRS_NUMBER);
    private final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    {
        cachedThreadPool.submit(
                () -> { // worker thread drain queue contained changed ccyPairs to
                    while(!Thread.interrupted()){
                        try {
                            final String ccyPair = ccyPairsChanged.take();
                            cachedThreadPool.submit(
                                  () -> priceProcessors.forEach(
                                          priceProcessor -> cachedThreadPool.submit(
                                                  () -> {
                                                      ccyPairAlreadyInQueue.put(ccyPair,false);
                                                      priceProcessor.onPrice(ccyPair,lots.get(ccyPair));
                                                  }
                                          )
                                  )
                            );
                        } catch (InterruptedException e) {
                            System.out.println("worker thread was interrupted and now gonna shutdown");
                        }
                    }
                }
        );
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        lots.put(ccyPair,rate);
        ccyPairAlreadyInQueue.compute(
                ccyPair,(key, isInQueue) -> {
                    if(isInQueue==null || !isInQueue){
                        ccyPairsChanged.add(ccyPair);
                    }
                    return true;
                });

    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        priceProcessors.add(priceProcessor);
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        priceProcessors.remove(priceProcessor);
    }
}
