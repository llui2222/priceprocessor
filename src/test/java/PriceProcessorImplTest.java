import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

class PriceProcessorImplTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void onPrice() throws InterruptedException {
        PriceProcessor priceProcessor = new PriceProcessorImpl();
        for (int i = 0; i < 10; i++) {
            int number = i;
            priceProcessor.subscribe(new PriceProcessor() {
                @Override
                public void onPrice(String ccyPair, double rate) {
                    System.out.println("PriceProcessor "+number + " processing " + ccyPair + " with rate "+rate);
                }

                @Override
                public void subscribe(PriceProcessor priceProcessor) {

                }

                @Override
                public void unsubscribe(PriceProcessor priceProcessor) {

                }
            });
        }
        String[] arrCcy = new String[]{"EURUSD", "EURRUB", "USDJPY"};
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        for (int i = 0; i < 100; i++) {
            priceProcessor.onPrice(
                    arrCcy[threadLocalRandom.nextInt(arrCcy.length)],
                    threadLocalRandom.nextDouble()
            );
        }
        Thread.sleep(3000);
    }
}